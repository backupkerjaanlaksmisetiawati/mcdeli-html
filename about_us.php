<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb bx-body-desktop">
    <div class="container mc-body-container">
        <div class="row">
            <div class="col-md-7 bx-about-us-history">
                <h2 class="header-t2">History</h2>
                <h4 class="sub-header-t2">歴史</h4>
                <p>PT MCdelica Food Indonesia established in May 2019 by Mitsubishi Corporation who has technology and know-how of food infustry in Japan and PT Mitra Pangan Pratama who has Indonesian R&D know-how and production experience. Factory is in Modern Cikande in Serang, Banten Indonesia which is around 60km away from Central Jakarta Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper.</p>
            </div>
        </div>
    </div>

    <div id="about-us-gallery" class="owl-carousel owl-theme about-us-gallery">
        <?php for($x=0;$x<=10;$x++) { ?>
            <img class="owl-lazy" data-src="assets/img/img-history-1.jpg" alt="">
            <img class="owl-lazy" data-src="assets/img/img-history-2.jpg" alt="">
        <?php } ?>
    </div>

    <div class="bx-about-us-vision">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="header-t2">Vision</h2>
                    <h4 class="sub-header-t2">ビジョン</h4>
                </div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-vision.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex align-items-center">
                    <h4>Bring Happiness and ConvEnience to the People through Delicious & Affordable Food</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-about-us-mission">
        <div class="container mc-body-container">
            <div>
                <h2 class="header-t2">Mission</h2>
                <h4 class="sub-header-t2">ミッション</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-mission-1-orange.png" alt="">
                    </div>
                    <div>
                        <p>
                            Continous Improvement of our Product through 
                            Japanese Food Technology and Innovation
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-mission-2-orange.png" alt="">
                    </div>
                    <div>
                        <p>
                            Introducing New Food Experience from all over the world
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-mission-3-orange.png" alt="">
                    </div>
                    <div>
                        <p>
                            No Compromise on Food Safety &amp; Halal Standart
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-about-us-commitment">
        <div>
            <h2 class="header-t2">Our Commitment</h2>
            <h4 class="sub-header-t2">我々のコミットメント</h4>
        </div>
    </div>

    <div class="bx-about-us-commitment-list">
        <a>
            <h4>01</h4>
            <h3>Delicious</h3>
            <div>
                <p>
                    Provide delicious and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
        <a>
            <h4>02</h4>
            <h3>Something New</h3>
            <div>
                <p>
                    Provide something new and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
        <a>
            <h4>03</h4>
            <h3>Delicious</h3>
            <div>
                <p>
                    Provide affordable price and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
        <a>
            <h4>04</h4>
            <h3>Safety</h3>
            <div>
                <p>
                    Provide safe and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
    </div>
</section>

<section class="mobile-wb bx-body-mobile">
    <div class="container mc-body-container">
        <div class="row">
            <div class="col-md-7 bx-about-us-history">
                <h2 class="header-t2">History</h2>
                <h4 class="sub-header-t2">歴史</h4>
                <p>PT MCdelica Food Indonesia established in May 2019 by Mitsubishi Corporation who has technology and know-how of food infustry in Japan and PT Mitra Pangan Pratama who has Indonesian R&D know-how and production experience. Factory is in Modern Cikande in Serang, Banten Indonesia which is around 60km away from Central Jakarta Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper.</p>
            </div>
        </div>
    </div>

    <div id="about-us-gallery-mobile" class="owl-carousel owl-theme about-us-gallery">
        <?php for($x=0;$x<=10;$x++) { ?>
            <img class="owl-lazy" data-src="assets/img/img-history-1.jpg" alt="">
            <img class="owl-lazy" data-src="assets/img/img-history-2.jpg" alt="">
        <?php } ?>
    </div>

    <div class="bx-about-us-vision">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="header-t2">Vision</h2>
                    <h4 class="sub-header-t2">ビジョン</h4>
                    <p>&nbsp;</p>
                </div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-vision.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex align-items-center">
                    <h4>Bring Happiness and ConvEnience to the People through Delicious & Affordable Food</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-about-us-mission">
        <div class="container mc-body-container">
            <div>
                <h2 class="header-t2">Mission</h2>
                <h4 class="sub-header-t2">ミッション</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-mission-1-orange.png" alt="">
                    </div>
                    <div>
                        <p>
                            Continous Improvement of our Product through 
                            Japanese Food Technology and Innovation
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-mission-2-orange.png" alt="">
                    </div>
                    <div>
                        <p>
                            Introducing New Food Experience from all over the world
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-mission-3-orange.png" alt="">
                    </div>
                    <div>
                        <p>
                            No Compromise on Food Safety &amp; Halal Standart
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-about-us-commitment">
        <div>
            <h2 class="header-t2">Our Commitment</h2>
            <h4 class="sub-header-t2">我々のコミットメント</h4>
        </div>
    </div>

    <div class="bx-about-us-commitment-list">
        <a>
            <h4>01</h4>
            <h3>Delicious</h3>
            <div>
                <p>
                    Provide delicious and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
        <a>
            <h4>02</h4>
            <h3>Something New</h3>
            <div>
                <p>
                    Provide something new and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
    </div>

    <div class="bx-about-us-commitment-list">
        <a>
            <h4>03</h4>
            <h3>Delicious</h3>
            <div>
                <p>
                    Provide affordable price and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
        <a>
            <h4>04</h4>
            <h3>Safety</h3>
            <div>
                <p>
                    Provide safe and valuable products by securing 
                    the finest ingredients and ensuring their freshness
                </p>
            </div>
            <div>
                <img src="assets/img/arrow-right-thick.png" />
            </div>
        </a>
    </div>
</section>

<?php include 'templates/footer.php'; ?>