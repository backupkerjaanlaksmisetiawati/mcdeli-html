(function ($) {
    "use strict";

    $("a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){
                window.location.hash = hash;
            });
        }
    });

    if ($("#home-banner, #home-banner-mobile").length > 0) {
        var owl = $('#home-banner, #home-banner-mobile');
        owl.owlCarousel({
            lazyLoad: true,
            loop: false,
            nav: false,
            dots: false,
            // autoplay: true,
            items: 1
        });
        // owl.on('mousewheel', '.owl-stage', function (e) {
        //     if (e.deltaY > 0) {
        //         owl.trigger('next.owl');
        //     } else {
        //         owl.trigger('prev.owl');
        //     }
        //     e.preventDefault();
        // });
    };

    if ($("#about-us-gallery").length > 0) {
        var owl = $('#about-us-gallery');
        owl.owlCarousel({
            lazyLoad: true,
            loop: false,
            nav: true,
            dots: false,
            navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 2
                },
                960: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            }
        });
        // owl.on('mousewheel', '.owl-stage', function (e) {
        //     if (e.deltaY > 0) {
        //         owl.trigger('next.owl');
        //     } else {
        //         owl.trigger('prev.owl');
        //     }
        //     e.preventDefault();
        // });
    };

    if ($("#about-us-gallery-mobile").length > 0) {
        var owl = $('#about-us-gallery-mobile');
        owl.owlCarousel({
            lazyLoad: true,
            loop: false,
            nav: true,
            dots: false,
            navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 2
                },
                960: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            }
        });
        // owl.on('mousewheel', '.owl-stage', function (e) {
        //     if (e.deltaY > 0) {
        //         owl.trigger('next.owl');
        //     } else {
        //         owl.trigger('prev.owl');
        //     }
        //     e.preventDefault();
        // });
    };

    if ($("#home-products, #home-products-mobile").length > 0) {
        var owl = $('#home-products, #home-products-mobile');
        owl.owlCarousel({
            lazyLoad: true,
            loop: false,
            nav: true,
            dots: false,
            navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                960: {
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
        // owl.on('mousewheel', '.owl-stage', function (e) {
        //     if (e.deltaY > 0) {
        //         owl.trigger('next.owl');
        //     } else {
        //         owl.trigger('prev.owl');
        //     }
        //     e.preventDefault();
        // });
    };

    if ($("#recent-posts, #recent-posts-mobile").length > 0) {
        var owl = $('#recent-posts, #recent-posts-mobile');
        owl.owlCarousel({
            lazyLoad: true,
            loop: false,
            nav: true,
            slideBy: 2,
            dots: false,
            navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
            margin: 15,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                960: {
                    items: 2
                },
                1200: {
                    items: 2
                }
            }
        });
        // owl.on('mousewheel', '.owl-stage', function (e) {
        //     if (e.deltaY > 0) {
        //         owl.trigger('next.owl');
        //     } else {
        //         owl.trigger('prev.owl');
        //     }
        //     e.preventDefault();
        // });
    };

    if ($("#map").length > 0) {
        var platform = new H.service.Platform({
            'apikey': 'WSz01Bs8ag0lY4AceQroqOBgLtnlknpTlGX76LnaN4Y'
        });

        // Obtain the default map types from the platform object:
        var defaultLayers = platform.createDefaultLayers();

        // Instantiate (and display) a map object:
        var map = new H.Map(
            document.getElementById('map'),
            defaultLayers.vector.normal.map,
            {
                zoom: 13,
                center: { lat: -6.15106, lng: 106.88996 }
            }
        );

        // Create the default UI:
        var ui = H.ui.UI.createDefault(map, defaultLayers);
        // ui.getControl('zoom').setEnabled(true);
        // ui.getControl('scalebar').setEnabled(true);

        // Enable the event system on the map instance:
        var mapEvents = new H.mapevents.MapEvents(map);

        // Add event listeners:
        map.addEventListener('drag', function (evt) {
            // Log 'tap' and 'mouse' events:
            // console.log(evt.type, evt.currentPointer.type);
        });

        // Instantiate the default behavior, providing the mapEvents object:
        var behavior = new H.mapevents.Behavior(mapEvents);
    }

    if ($(".navigable, navigable-mobile").length > 0) {
        $('.navigable, navigable-mobile').contentNavigation({
            offsetIndicator: '-33%',
        });
    }

    $(".bx-menu-1 .hamburger").on("click", function (e) {
        e.preventDefault();
        var menu1 = $(".menu-1");
        if (menu1.hasClass("active")) {
            menu1.removeClass("active");
        } else {
            menu1.addClass("active");
        }
    });
})(jQuery);
