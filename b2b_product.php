<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb bx-body-desktop">
    <div class="container mc-body-container">
        <h2 class="header-t2">Our Product for Professional Use</h2>
        <div class="row">
            <div class="col-md-7 bx-about-us-history">
                <h4 class="sub-header-t2">業務用製品</h4>
                <p>
                    Especially for chain store operation, stability of 
                    taste and saving time for cooking is one of 
                    important factor. Our product will offer 
                    you these solution. Easy to cook and 
                    serve products will meet the demands 
                    of retailer and Horeca.
                </p>
            </div>
        </div>
    </div>

    <div class="bx-about-us-mission bx-b2b">
        <div class="container mc-body-container">
            <div>
                <h3 class="sub-header-sv">Save time, space and easy operation</h3>
            </div>
            <div class="row">
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-product-1-orange.png" alt="">
                    </div>
                    <div>
                        <h4>Frozen Products</h4>
                        <p>
                            We produce frozen products for Dessert/Snack/Sauce
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-product-2-orange.png" alt="">
                    </div>
                    <div>
                        <h4>Easy Operation</h4>
                        <p>
                            Thawing (Chilled/Reheat) &amp; Presentation
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-product-3-orange.png" alt="">
                    </div>
                    <div>
                        <h4>Original Menu</h4>
                        <p>
                            Always create great recipe for our original menu
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-rdp">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-5 d-flex align-items-center align-items-center">
                    <div>
                        <h2 class="header-t2">Original Menu Development</h2>
                        <h4 class="sub-header-t2 mr">製造</h4>
                        <p>
                            Our mission is to contribute your 
                            sales increase. After the deep discussion 
                            and direction from customer, we will 
                            propose your original food.
                        </p>
                        <a href="Javascript:;" class="btn-download">
                            <img src="assets/img/icon-download.png" />
                            <span>Download Product Catalog</span>
                            <hr />
                        </a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-download-menu.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-steps">
        <hr />
        <div class="container mc-body-container">
            <div class="step-list up">
                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>1</div>
                        </div>
                        <p>
                            Market Research &amp; Benchmark Setting
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>3</div>
                        </div>
                        <p>
                            Tasting and Discussion
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>5</div>
                        </div>
                        <p>
                            Mass Production Test
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list down">
                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>2</div>
                        </div>
                        <p>
                            Make prototype sample by R&amp;D team
                        </p>
                    </div>
                </div>

                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>4</div>
                        </div>
                        <p>
                            Menu Meeting with Customer
                        </p>
                    </div>
                </div>

                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>6</div>
                        </div>
                        <p>
                            Make mass production recipe
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb bx-body-mobile">
    <div class="container mc-body-container">
        <h2 class="header-t2">Our Product for Professional Use</h2>
        <div class="row">
            <div class="col-md-7 bx-about-us-history">
                <h4 class="sub-header-t2">業務用製品</h4>
                <p>
                    Especially for chain store operation, stability of 
                    taste and saving time for cooking is one of 
                    important factor. Our product will offer 
                    you these solution. Easy to cook and 
                    serve products will meet the demands 
                    of retailer and Horeca.
                </p>
            </div>
        </div>
    </div>

    <div class="bx-about-us-mission bx-b2b">
        <div class="container mc-body-container">
            <div>
                <h3 class="sub-header-sv">Save time, space and easy operation</h3>
            </div>
            <div class="row">
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-product-1-orange.png" alt="">
                    </div>
                    <div>
                        <h4>Frozen Products</h4>
                        <p>
                            We produce frozen products for Dessert/Snack/Sauce
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-product-2-orange.png" alt="">
                    </div>
                    <div>
                        <h4>Easy Operation</h4>
                        <p>
                            Thawing (Chilled/Reheat) &amp; Presentation
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-mission-list">
                    <div>
                        <img src="assets/img/icon-product-3-orange.png" alt="">
                    </div>
                    <div>
                        <h4>Original Menu</h4>
                        <p>
                            Always create great recipe for our original menu
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-rdp">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-12 d-flex align-items-center align-items-center">
                    <div>
                        <h2 class="header-t2">Original Menu Development</h2>
                        <h4 class="sub-header-t2 mr">製造</h4>
                        <p>
                            Our mission is to contribute your 
                            sales increase. After the deep discussion 
                            and direction from customer, we will 
                            propose your original food.
                        </p>
                    </div>
                </div>
                <div class="col-md-12 image">
                    <div>
                        <div>
                            <img src="assets/img/img-download-menu.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <a href="Javascript:;" class="btn-download">
                        <img src="assets/img/icon-download.png" />
                        <span>Download Product Catalog</span>
                        <hr />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container mc-body-container">
        <div class="bx-rdp-steps-mobile">
            <hr class="rdp-steps-mobile-border" />

            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Market Research &amp; Benchmark Setting
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>1</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>2</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Make prototype sample by R&amp;D team
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Tasting and Discussion
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>3</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>4</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Menu Meeting with Customer
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Mass Production Test
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>5</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>6</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Make mass production recipe
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>