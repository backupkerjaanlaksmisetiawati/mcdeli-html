<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<div id="map"></div>

<section class="desktop-wb position-relative">
    <div class="container mc-contact-container">
        <div class="bx-contact-us">
            <div class="row align-items-end">
                <div class="col-md-6">
                    <form>
                        <h3 class="contact-us-t">Feel free to contact us</h3>
                        
                        <div class="row">
                            <div class="col-md-6 field-group">
                                <label>First Name</label>
                                <input type="text" class="field-input" />
                            </div>
                            <div class="col-md-6 field-group">
                                <label>Last Name</label>
                                <input type="text" class="field-input" />
                            </div>
                        </div>
                        
                        <div class="field-group">
                            <label>Email Address</label>
                            <input type="text" class="field-input" />
                        </div>
                        
                        <div class="field-group">
                            <label>Message</label>
                            <textarea class="field-input"></textarea>
                        </div>

                        <div class="field-group">
                            <div class="row justify-content-end">
                                <div class="col-md-6">
                                    <button class="btn-contact">
                                        <span>Submit</span>
                                        <hr />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-1"></div>
                
                <div class="col-md-5 contact-address">
                    <h4>HEAD OFFICE</h4>

                    <p>
                        PT MC DELICA FOOD INDONESIA <br />
                        Jl. Raya Jakarta Serang KM 68 Cikande, Nambo Ilir Kibin, <br />
                        Kab. Serang Banten 4286
                    </p>

                    <h4>Phone</h4>

                    <p>+62 21 12345678</p>

                    <h4>Email</h4>

                    <p>info@mcdeli.com</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb">        
    <div class="container">
        <div class="bx-contact-us">
            <div class="row align-items-end">
                <div class="col-md-5 contact-address">
                    <h4>HEAD OFFICE</h4>

                    <p>
                        PT MC DELICA FOOD INDONESIA <br />
                        Jl. Raya Jakarta Serang KM 68 Cikande, Nambo Ilir Kibin, <br />
                        Kab. Serang Banten 4286
                    </p>

                    <h4>Phone</h4>

                    <p>+62 21 12345678</p>

                    <h4>Email</h4>

                    <p>info@mcdeli.com</p>
                </div>
                <div class="col-md-6">
                    <form>
                        <h3 class="contact-us-t">Feel free to contact us</h3>
                        
                        <div class="row">
                            <div class="col-md-6 field-group">
                                <label>First Name</label>
                                <input type="text" class="field-input" />
                            </div>
                            <div class="col-md-6 field-group">
                                <label>Last Name</label>
                                <input type="text" class="field-input" />
                            </div>
                        </div>
                        
                        <div class="field-group">
                            <label>Email Address</label>
                            <input type="text" class="field-input" />
                        </div>
                        
                        <div class="field-group">
                            <label>Message</label>
                            <textarea class="field-input"></textarea>
                        </div>

                        <div class="field-group">
                            <div class="row justify-content-end">
                                <div class="col-md-6">
                                    <button class="btn-contact">
                                        <span>Submit</span>
                                        <hr />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>