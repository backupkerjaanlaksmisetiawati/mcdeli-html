<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb">
    <div class="bx-home-banner">
        <div id="home-banner" class="owl-carousel owl-theme home-banner">
            <?php for($x=0;$x<=10;$x++) { ?>
                <div>
                    <div class="row banner-content">
                        <div class="col-md-7">
                            <p>Let's Discover Your Taste</p>
                            <h2>We Bring Happiness<br />And Convenience To<br />Your Belly</h2>
                            <a href="about_us.php" class="btn-banner">
                                <span>Read More</span>
                                <hr>
                            </a>
                        </div>
                    </div>
                    <img class="owl-lazy" data-src="assets/img/banner-2.png" alt="" />
                </div>
                <div>
                    <div class="row banner-content">
                        <div class="col-md-7">
                            <p>Let's Discover Your Taste</p>
                            <h2>We Bring Happiness<br />And Convenience To<br />Your Belly</h2>
                            <a href="about_us.php" class="btn-banner">
                                <span>Read More</span>
                                <hr>
                            </a>
                        </div>
                    </div>
                    <img class="owl-lazy" data-src="assets/img/banner-2.png" alt="" />
                </div>
            <?php } ?>
        </div>
        <a href="#our-retail-products" class="btn-scroll">
            <img src="assets/img/icon-scroll.png" alt="" />
            <p>Scroll Down</p>
        </a>
    </div>

    <div id="our-retail-products" class="home-products">
        <div class="container">
            <b><h2 class="header-t2">Our Retail Products</h2></b>
            <h4 class="sub-header-t2">私たちの小売製品</h4>
        </div>
        <div id="home-products" class="owl-carousel owl-theme home-products">
            <?php for($x=0;$x<=10;$x++) { ?>
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-1.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-1.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Fried Rice</span>
                        <span>チャーハン</span>
                    </p>
                </div>
                
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-2.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-2.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Onigiri</span>
                        <span>おにぎり</span>
                    </p>
                </div>
                
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-3.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-3.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Spaghetti &amp; Lasagna</span>
                        <span>スパゲッティとラザニア</span>
                    </p>
                </div>
                
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-4.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-4.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Steak</span>
                        <span>ステーキ</span>
                    </p>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="home-about">
        <img src="assets/img/float-3.png" class="float-3" />
        
        <img src="assets/img/float-5.png" class="float-5" />

        <div class="container mc-body-container">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <h4>
                        PT MCdelica Food Indonesia established in 
                        May 2019 by Mitsubishi Corporation who has 
                        technology and know-how of food industry in Japan and 
                        PT Mitra Pangan Pratama who has Indonesian
                        R&amp;D know-how and production experience.
                    </h4>
                    <p>
                        Factory is in Modern Cikande in Serang, 
                        Banten Indonesia which is around 
                        60km away from Central Jakarta
                    </p>
                    <p>&nbsp;</p>
                    <a href="about_us.php" class="btn-news">
                        <span>Read More</span>
                        <hr />
                    </a>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 col-ha-img">
                    <div>
                        <img src="assets/img/img-home-about.jpg" />
                    </div>
                    <p class="rtt-title">
                        <span>About Us</span>
                        <span>私たちの小売製品</span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="home-news">
        <img src="assets/img/float-2.png" class="float-2" />
        
        <img src="assets/img/float-1.png" class="float-1" />
        
        <img src="assets/img/float-4.png" class="float-4" />

        <div class="container mc-body-container">
            <h2 class="header-t2 text-center">News &amp; Event</h2>
            <h4 class="sub-header-t2 text-center">ニュース&amp;イベント</h4>
            <p class="mb-5">&nbsp;</p>

            <div class="bx-news-event left">
                <div class="row">
                    <div class="col-md-6 image">
                        <div>
                            <div>
                                <img src="assets/img/img-news-1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5 d-flex align-items-center">
                        <div>
                            <h3 class="title">Learn how to make onigiri</h2>
                            <h4 class="date">29 Sep 2020</h4>
                            <div class="short-c">
                                <p>
                                    Nullam quis risus eget urna mollis 
                                    ornare vel eu leo. Fusce dapibus, tellus 
                                    ac cursus commodo, tortor mauris 
                                    condimentum nibh, ut fermentum massa 
                                    justo sit amet risus. Aenean lacinia..
                                    bibendum nulla sed consectetur. Nullam 
                                    id dolor id nibh ultricies vehicula ut 
                                    id elit. Vivamus sagittis lacus vel 
                                    augue laoreet rutrum faucibus dolor auctor...
                                </p>
                            </div>
                            <a href="news_event_detail.php" class="btn-news">
                                <span>Read More</span>
                                <hr />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="bx-news-event right">
                <div class="row">
                    <div class="col-md-5 d-flex align-items-center text-right">
                        <div>
                            <h3 class="title">Create Japanese Bento Set</h2>
                            <h4 class="date">29 Sep 2020</h4>
                            <div class="short-c">
                                <p>
                                    Nullam quis risus eget urna mollis 
                                    ornare vel eu leo. Fusce dapibus, tellus 
                                    ac cursus commodo, tortor mauris 
                                    condimentum nibh, ut fermentum massa 
                                    justo sit amet risus. Aenean lacinia..
                                    bibendum nulla sed consectetur. Nullam 
                                    id dolor id nibh ultricies vehicula ut 
                                    id elit. Vivamus sagittis lacus vel 
                                    augue laoreet rutrum faucibus dolor auctor...
                                </p>
                            </div>
                            <a href="news_event_detail.php" class="btn-news">
                                <span>Read More</span>
                                <hr />
                            </a>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6 image">
                        <div>
                            <div>
                                <img src="assets/img/img-news-2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="bx-news-event left">
                <div class="row">
                    <div class="col-md-6 image">
                        <div>
                            <div>
                                <img src="assets/img/img-news-3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5 d-flex align-items-center">
                        <div>
                            <h3 class="title">Fluffy and creamy Dessert</h2>
                            <h4 class="date">29 Sep 2020</h4>
                            <div class="short-c">
                                <p>
                                    Nullam quis risus eget urna mollis 
                                    ornare vel eu leo. Fusce dapibus, tellus 
                                    ac cursus commodo, tortor mauris 
                                    condimentum nibh, ut fermentum massa 
                                    justo sit amet risus. Aenean lacinia..
                                    bibendum nulla sed consectetur. Nullam 
                                    id dolor id nibh ultricies vehicula ut 
                                    id elit. Vivamus sagittis lacus vel 
                                    augue laoreet rutrum faucibus dolor auctor...
                                </p>
                            </div>
                            <a href="news_event_detail.php" class="btn-news">
                                <span>Read More</span>
                                <hr />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb">
    <div class="bx-home-banner">
        <div id="home-banner-mobile" class="owl-carousel owl-theme home-banner">
            <?php for($x=0;$x<=10;$x++) { ?>
                <a href=""><img class="owl-lazy" data-src="assets/img/banner-1.png" alt="" /></a>
                <a href=""><img class="owl-lazy" data-src="assets/img/banner-2.png" alt="" /></a>
            <?php } ?>
        </div>
    </div>

    <div id="our-retail-products" class=bx-body-desktop">
        <div class="container">
            <h2 class="header-t2">Our Retail Products</h2>
            <h4 class="sub-header-t2">私たちの小売製品</h4>
        </div>
        
        <div id="home-products-mobile" class="owl-carousel owl-theme home-products">
            <?php for($x=0;$x<=10;$x++) { ?>
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-1.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-1.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Fried Rice</span>
                        <span>チャーハン</span>
                    </p>
                </div>
                
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-2.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-2.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Onigiri</span>
                        <span>おにぎり</span>
                    </p>
                </div>
                
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-3.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-3.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Spaghetti &amp; Lasagna</span>
                        <span>スパゲッティとラザニア</span>
                    </p>
                </div>
                
                <div class="col-retail-product">
                    <div class="product-img" style="background-image: url('assets/img/img-retail-product-4.jpg');">
                        <div>
                            <img src="assets/img/img-retail-product-hover-4.png" class="mw-100" />
                        </div>
                        <a href="retail_product_detail.php" class="btn-retprod">
                            <span>View Details</span>
                            <hr />
                        </a>
                    </div>
                    <p class="product-nm">
                        <span>Steak</span>
                        <span>ステーキ</span>
                    </p>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="home-about">
        <div class="container mc-body-container">
            <div class="row align-items-center">
                <div class="col-md-12 col-ha-img">
                    <div>
                        <img src="assets/img/img-home-about.jpg" />
                    </div>
                    <p class="rtt-title">
                        <span>About Us</span>
                        <span>私たちの小売製品</span>
                    </p>
                </div>
                <div class="col-md-12">
                    <h4>
                        PT MCdelica Food Indonesia established in 
                        May 2019 by Mitsubishi Corporation who has 
                        technology and know-how of food industry in Japan and 
                        PT Mitra Pangan Pratama who has Indonesian
                        R&amp;D know-how and production experience.
                    </h4>
                    <p>
                        Factory is in Modern Cikande in Serang, 
                        Banten Indonesia which is around 
                        60km away from Central Jakarta
                    </p>
                    <p>&nbsp;</p>
                    <a href="about_us.php" class="btn-news">
                        <span>Read More</span>
                        <hr />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-body-desktop">
        <div class="container mc-body-container">
            <h2 class="header-t2 text-center">News &amp; Event</h2>
            <h4 class="sub-header-t2 text-center">ニュース&amp;イベント</h4>
            <p>&nbsp;</p>

            <div class="bx-news-event left">
                <div class="row">
                    <div class="col-sm-4 image">
                        <div>
                            <div>
                                <img src="assets/img/img-news-1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 d-flex align-items-center">
                        <div>
                            <h3 class="title">Learn how to make onigiri</h2>
                            <h4 class="date">29 Sep 2020</h4>
                            <div class="short-c">
                                <p>
                                    Nullam quis risus eget urna mollis 
                                    ornare vel eu leo. Fusce dapibus, tellus 
                                    ac cursus commodo, tortor mauris 
                                    condimentum nibh, ut fermentum massa 
                                    justo sit amet risus. Aenean lacinia..
                                    bibendum nulla sed consectetur. Nullam 
                                    id dolor id nibh ultricies vehicula ut 
                                    id elit. Vivamus sagittis lacus vel 
                                    augue laoreet rutrum faucibus dolor auctor...
                                </p>
                            </div>
                            <a href="news_event_detail.php" class="btn-news">
                                <span>Read More</span>
                                <hr />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="bx-news-event right">
                <div class="row">
                    <div class="col-sm-4 image">
                        <div>
                            <div>
                                <img src="assets/img/img-news-2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 d-flex align-items-center">
                        <div>
                            <h3 class="title">Create Japanese Bento Set</h2>
                            <h4 class="date">29 Sep 2020</h4>
                            <div class="short-c">
                                <p>
                                    Nullam quis risus eget urna mollis 
                                    ornare vel eu leo. Fusce dapibus, tellus 
                                    ac cursus commodo, tortor mauris 
                                    condimentum nibh, ut fermentum massa 
                                    justo sit amet risus. Aenean lacinia..
                                    bibendum nulla sed consectetur. Nullam 
                                    id dolor id nibh ultricies vehicula ut 
                                    id elit. Vivamus sagittis lacus vel 
                                    augue laoreet rutrum faucibus dolor auctor...
                                </p>
                            </div>
                            <a href="news_event_detail.php" class="btn-news">
                                <span>Read More</span>
                                <hr />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="bx-news-event left">
                <div class="row">
                    <div class="col-sm-4 image">
                        <div>
                            <div>
                                <img src="assets/img/img-news-3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 d-flex align-items-center">
                        <div>
                            <h3 class="title">Fluffy and creamy Dessert</h2>
                            <h4 class="date">29 Sep 2020</h4>
                            <div class="short-c">
                                <p>
                                    Nullam quis risus eget urna mollis 
                                    ornare vel eu leo. Fusce dapibus, tellus 
                                    ac cursus commodo, tortor mauris 
                                    condimentum nibh, ut fermentum massa 
                                    justo sit amet risus. Aenean lacinia..
                                    bibendum nulla sed consectetur. Nullam 
                                    id dolor id nibh ultricies vehicula ut 
                                    id elit. Vivamus sagittis lacus vel 
                                    augue laoreet rutrum faucibus dolor auctor...
                                </p>
                            </div>
                            <a href="news_event_detail.php" class="btn-news">
                                <span>Read More</span>
                                <hr />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</section>

<?php include 'templates/footer.php'; ?>