<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb bx-body-desktop">
    <div class="container mc-body-container">
        <div class="bx-news-event left">
            <div class="row">
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-1.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 d-flex align-items-center">
                    <div>
                        <h3 class="title">Learn how to make onigiri</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bx-news-event right">
            <div class="row">
                <div class="col-md-5 d-flex align-items-center text-right">
                    <div>
                        <h3 class="title">Create Japanese Bento Set</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-2.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bx-news-event left">
            <div class="row">
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-3.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 d-flex align-items-center">
                    <div>
                        <h3 class="title">Fluffy and creamy Dessert</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bx-news-event right">
            <div class="row">
                <div class="col-md-5 d-flex align-items-center text-right">
                    <div>
                        <h3 class="title">Create Japanese Bento Set</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-2.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bx-news-page">
            <a href="news_event.php?page=1" <?php if( !isset($_GET["page"]) || (isset($_GET["page"]) && $_GET["page"]==1 ) ) { ?>class="active"<?php } ?>>
                1
            </a>
            <a href="news_event.php?page=2" <?php if( isset($_GET["page"]) && $_GET["page"]==2 ) { ?>class="active"<?php } ?>>
                2
            </a>
            <a href="news_event.php?page=3" <?php if( isset($_GET["page"]) && $_GET["page"]==3 ) { ?>class="active"<?php } ?>>
                3
            </a>
            <a href="news_event.php?page=4" <?php if( isset($_GET["page"]) && $_GET["page"]==4 ) { ?>class="active"<?php } ?>>
                4
            </a>
            <a href="news_event.php?page=5" <?php if( isset($_GET["page"]) && $_GET["page"]==5 ) { ?>class="active"<?php } ?>>
                5
            </a>
            <a href="news_event.php?page=6" <?php if( isset($_GET["page"]) && $_GET["page"]==6 ) { ?>class="active"<?php } ?>>
                6
            </a>
        </div>
    </div>
</section>

<section class="mobile-wb bx-body-mobile">
    <div class="container mc-body-container">
        <div class="bx-news-event left">
            <div class="row">
                <div class="col-sm-4 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-1.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 d-flex align-items-center">
                    <div>
                        <h3 class="title">Learn how to make onigiri</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bx-news-event right">
            <div class="row">
                <div class="col-sm-4 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-2.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 d-flex align-items-center">
                    <div>
                        <h3 class="title">Create Japanese Bento Set</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bx-news-event left">
            <div class="row">
                <div class="col-sm-4 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-3.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 d-flex align-items-center">
                    <div>
                        <h3 class="title">Fluffy and creamy Dessert</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bx-news-event right">
            <div class="row">
                <div class="col-sm-4 image">
                    <div>
                        <div>
                            <img src="assets/img/img-news-2.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 d-flex align-items-center">
                    <div>
                        <h3 class="title">Create Japanese Bento Set</h2>
                        <h4 class="date">29 Sep 2020</h4>
                        <div class="short-c">
                            <p>
                                Nullam quis risus eget urna mollis 
                                ornare vel eu leo. Fusce dapibus, tellus 
                                ac cursus commodo, tortor mauris 
                                condimentum nibh, ut fermentum massa 
                                justo sit amet risus. Aenean lacinia..
                                bibendum nulla sed consectetur. Nullam 
                                id dolor id nibh ultricies vehicula ut 
                                id elit. Vivamus sagittis lacus vel 
                                augue laoreet rutrum faucibus dolor auctor...
                            </p>
                        </div>
                        <a href="news_event_detail.php" class="btn-news">
                            <span>Read More</span>
                            <hr />
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="bx-news-page">
            <a href="news_event.php?page=1" <?php if( !isset($_GET["page"]) || (isset($_GET["page"]) && $_GET["page"]==1 ) ) { ?>class="active"<?php } ?>>
                1
            </a>
            <a href="news_event.php?page=2" <?php if( isset($_GET["page"]) && $_GET["page"]==2 ) { ?>class="active"<?php } ?>>
                2
            </a>
            <a href="news_event.php?page=3" <?php if( isset($_GET["page"]) && $_GET["page"]==3 ) { ?>class="active"<?php } ?>>
                3
            </a>
            <a href="news_event.php?page=4" <?php if( isset($_GET["page"]) && $_GET["page"]==4 ) { ?>class="active"<?php } ?>>
                4
            </a>
            <a href="news_event.php?page=5" <?php if( isset($_GET["page"]) && $_GET["page"]==5 ) { ?>class="active"<?php } ?>>
                5
            </a>
            <a href="news_event.php?page=6" <?php if( isset($_GET["page"]) && $_GET["page"]==6 ) { ?>class="active"<?php } ?>>
                6
            </a>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>