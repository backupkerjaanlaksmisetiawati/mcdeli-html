<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb">
    <img src="assets/img/banner-news-detail.jpg" class="w-100" />

    <div class="container mc-body-container">
        <div class="bx-body-desktop">
            <div class="news-breadcrump">
                <a href="news_event.php">News and Event</a>
                <span>></span>
                <a href="news_event_detail.php">Learn how to make onigiri</a>
            </div>

            <h1 class="news-title">
                Learn how to make onigiri
            </h1>

            <p class="news-date">
                29 Sep 2020
            </p>

            <div class="news-content">
                <p>
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. 
                </p>
                <p>
                    Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. <br/>
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                </p>

                <p>
                    <img src="assets/img/img-news-detail.jpg" class="mw-100" />
                </p>

                <p>
                    Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo.
                    <br />Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                </p>
                <p>
                    Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo.
                </p>
            </div>
        </div>

        <div class="bx-recent-posts">
            <h2>Recently Posts</h2>
            <div id="recent-posts" class="owl-carousel owl-theme recent-posts">
                <?php for($x=0;$x<=10;$x++) { ?>
                    <a href="Javascript:;" class="col-recent-post left">
                        <div class="image">
                            <div>
                                <div>
                                    <img src="assets/img/img-news-1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div>
                            <h3>Fluffy and Creamy Dessert</h3>
                            <p>29 Sep 2020</p>
                        </div>
                    </a>
                    <a href="Javascript:;" class="col-recent-post right">
                        <div>
                            <h3>Fluffy and Creamy Dessert</h3>
                            <p>29 Sep 2020</p>
                        </div>
                        <div class="image">
                            <div>
                                <div>
                                    <img src="assets/img/img-news-2.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb">
    <img src="assets/img/banner-news-detail.jpg" class="w-100" />

    <div class="container mc-body-container">
        <div class="bx-body-mobile">
            <div class="news-breadcrump">
                <a href="news_event.php">News and Event</a>
                <span>></span>
                <a href="news_event_detail.php">Learn how to make onigiri</a>
            </div>

            <h1 class="news-title">
                Learn how to make onigiri
            </h1>

            <p class="news-date">
                29 Sep 2020
            </p>

            <div class="news-content">
                <p>
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. 
                </p>
                <p>
                    Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. <br/>
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                </p>

                <p>
                    <img src="assets/img/img-news-detail.jpg" class="mw-100" />
                </p>

                <p>
                    Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo.
                    <br />Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus mollis interdum. Nullam id dolor id nibh ultricies vehicula ut id elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                </p>
                <p>
                    Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo.
                </p>
            </div>
        </div>

        <div class="bx-recent-posts">
            <h2>Recently Posts</h2>
            <div id="recent-posts-mobile" class="owl-carousel owl-theme recent-posts">
                <?php for($x=0;$x<=10;$x++) { ?>
                    <a href="Javascript:;" class="col-recent-post left">
                        <div class="image">
                            <div>
                                <div>
                                    <img src="assets/img/img-news-1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div>
                            <h3>Fluffy and Creamy Dessert</h3>
                            <p>29 Sep 2020</p>
                        </div>
                    </a>
                    <a href="Javascript:;" class="col-recent-post left">
                        <div class="image">
                            <div>
                                <div>
                                    <img src="assets/img/img-news-2.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div>
                            <h3>Fluffy and Creamy Dessert</h3>
                            <p>29 Sep 2020</p>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>