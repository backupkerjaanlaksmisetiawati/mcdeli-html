<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb bx-body-desktop">
    <div class="container mc-body-container bx-qc">
        <div class="row">
            <div class="col-md-6 image">
                <div>
                    <div>
                        <img src="assets/img/img-qc.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5 d-flex align-items-center align-items-center">
                <div>
                    <h2 class="header-t2">Quality Control</h2>
                    <h4 class="sub-header-t2">品質管理</h4>
                    <h2 class="head-text">
                        Delicious Food Meets Japanese Technology 
                    </h2>
                    <p>
                        Raw Material 
                        <br />- Traceability of Raw Material 
                        <br />- Control the Raw Material Expiry date
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-rdp">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-5 d-flex align-items-center align-items-center">
                    <div>
                        <h2 class="header-t2">Quality Assurance</h2>
                        <h4 class="sub-header-t2 mr">製造</h4>
                        <p>
                            We brings know-how and experience from 
                            food manufacturing for convenience stores 
                            in Japan. Its Japanese technology enable us 
                            to do mass production with stable 
                            quality and hygiene. This will 
                            accomplish the delicious and affordable 
                            price for our valuable customers.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-qa.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-steps bx-rdp-steps-qa">
        <hr />
        <div class="container">
            <div class="step-list up">
                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>1</div>
                        </div>
                        <p>
                            Checking body temperature
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>3</div>
                        </div>
                        <p>
                            Air Shower
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>5</div>
                        </div>
                        <p>
                            Metal Detector
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>7</div>
                        </div>
                        <p>
                            Analise product about bacteria
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list down">
                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>2</div>
                        </div>
                        <p>
                            Change cloth, use cap, mask and hand gloves
                        </p>
                    </div>
                </div>

                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>4</div>
                        </div>
                        <p>
                            Check temperature inside product
                        </p>
                    </div>
                </div>

                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>6</div>
                        </div>
                        <p>
                            QC Check with Japanese Standard
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb bx-body-mobile">
    <div class="container mc-body-container bx-qc">
        <div class="row">
            <div class="col-md-12 d-flex align-items-center align-items-center">
                <div>
                    <h2 class="header-t2">Quality Control</h2>
                    <h4 class="sub-header-t2">品質管理</h4>
                    <h2 class="head-text">
                        Delicious Food Meets Japanese Technology 
                    </h2>
                    <p>
                        Raw Material 
                        <br />- Traceability of Raw Material 
                        <br />- Control the Raw Material Expiry date
                    </p>
                </div>
            </div>
            <div class="col-md-12 image">
                <div>
                    <div>
                        <img src="assets/img/img-qc.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-rdp">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-12 d-flex align-items-center align-items-center">
                    <div>
                        <h2 class="header-t2">Quality Assurance</h2>
                        <h4 class="sub-header-t2 mr">製造</h4>
                        <p>
                            We brings know-how and experience from 
                            food manufacturing for convenience stores 
                            in Japan. Its Japanese technology enable us 
                            to do mass production with stable 
                            quality and hygiene. This will 
                            accomplish the delicious and affordable 
                            price for our valuable customers.
                        </p>
                    </div>
                </div>
                <div class="col-md-12 image">
                    <div>
                        <div>
                            <img src="assets/img/img-qa.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mc-body-container mb-5">
        <div class="bx-rdp-steps-mobile">
            <hr class="rdp-steps-mobile-border" />

            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Checking body temperature
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>1</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>2</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Change cloth, use cap, mask and hand gloves
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Air Shower
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>3</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>4</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Check temperature inside product
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Metal Detector
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>5</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>6</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                QC Check with Japanese Standard
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Analise product about bacteria
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>7</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>