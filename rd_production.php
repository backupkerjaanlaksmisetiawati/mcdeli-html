<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb bx-body-desktop">
    <div class="container mc-body-container bx-rnd">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center align-items-center">
                <img src="assets/img/img-rd.jpg" class="w-100" />
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5 col-rdp-rd">
                <div>
                    <h2 class="header-t2">R&amp;D</h2>
                    <h4 class="sub-header-t2">研究開発</h4>
                    <h2>
                        Delicious Food Meets Japanese Technology 
                    </h2>
                    <p>
                        We are developing delicious foods using 
                        recipe from over the world. We have not only 
                        Indonesian chef but also Japanese Chef who knows 
                        thousand of food experience in the world. 
                        Indonesian People has own taste preference 
                        therefore we always develop food which 
                        can match to its preference.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-rdp">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-5 d-flex align-items-center align-items-center">
                    <div>
                        <h2 class="header-t2">Production</h2>
                        <h4 class="sub-header-t2 mr">製造</h4>
                        <p>
                            We brings know-how and experience from 
                            food manufacturing for convenience stores 
                            in Japan. Its Japanese technology enable us 
                            to do mass production with stable 
                            quality and hygiene. This will 
                            accomplish the delicious and affordable 
                            price for our valuable customers.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 image">
                    <div>
                        <div>
                            <img src="assets/img/img-rd-production.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-steps">
        <hr />
        <div class="container mc-body-container">
            <div class="step-list up">
                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>1</div>
                        </div>
                        <p>
                            Washing fresh Rice with purified water by filter
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>3</div>
                        </div>
                        <p>
                            Cooking rice by machine
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="step-item">
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>5</div>
                        </div>
                        <p>
                            Plating on box
                        </p>
                    </div>
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list down">
                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>2</div>
                        </div>
                        <p>
                            Washing fresh Rice with purified water by filter
                        </p>
                    </div>
                </div>

                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>4</div>
                        </div>
                        <p>
                            Cooking rice by machine
                        </p>
                    </div>
                </div>

                <div class="step-item">
                    <div class="line">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                    </div>
                    <div class="content">
                        <div class="icon-step">
                            <div>6</div>
                        </div>
                        <p>
                            Plating on box
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb bx-body-mobile">
    <div class="container mc-body-container">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center align-items-center">
                <img src="assets/img/img-rd.jpg" class="w-100" />
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5 col-rdp-rd">
                <div>
                    <h2 class="header-t2">R&amp;D</h2>
                    <h4 class="sub-header-t2">研究開発</h4>
                    <h2>
                        Delicious Food Meets Japanese Technology 
                    </h2>
                    <p>
                        We are developing delicious foods using 
                        recipe from over the world. We have not only 
                        Indonesian chef but also Japanese Chef who knows 
                        thousand of food experience in the world. 
                        Indonesian People has own taste preference 
                        therefore we always develop food which 
                        can match to its preference.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-rdp-rdp">
        <div class="container mc-body-container">
            <div class="row">
                <div class="col-md-12 d-flex align-items-center align-items-center">
                    <div>
                        <h2 class="header-t2">Production</h2>
                        <h4 class="sub-header-t2 mr">製造</h4>
                        <p>
                            We brings know-how and experience from 
                            food manufacturing for convenience stores 
                            in Japan. Its Japanese technology enable us 
                            to do mass production with stable 
                            quality and hygiene. This will 
                            accomplish the delicious and affordable 
                            price for our valuable customers.
                        </p>
                    </div>
                </div>
                <div class="col-md-12 image">
                    <div>
                        <div>
                            <img src="assets/img/img-rd-production.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mc-body-container mb-5">
        <div class="bx-rdp-steps-mobile">
            <hr class="rdp-steps-mobile-border" />

            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Washing fresh Rice with purified water by filter
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>1</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>2</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Washing fresh Rice with purified water by filter
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Cooking rice by machine
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>3</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>4</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Cooking rice by machine
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-list">
                <div class="step-item brdr left">
                    <div class="bx-content-image">
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Plating on box
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="round">
                            <div>5</div>
                        </div>
                    </div>
                </div>
                <div class="step-item right">&nbsp;</div>
            </div>
            
            <div class="step-list">
                <div class="step-item left">&nbsp;</div>
                <div class="step-item brdr right">
                    <div class="bx-content-image">
                        <div class="round">
                            <div>6</div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <div class="image">
                                <img src="assets/img/img-rd-step.jpg" class="w-100" alt="">
                            </div>
                            <div class="content">
                                Plating on box
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>