<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb bx-body-desktop">
    <div class="container mc-body-container">
        <div class="row">
            <div class="col-md-7 bx-about-us-history">
                <h2 class="header-t2">Our Retail Products</h2>
                <h4 class="sub-header-t2">私たちの小売製品</h4>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-1.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-1.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Fried Rice</span>
                    <span>チャーハン</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-2.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-2.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Onigiri</span>
                    <span>おにぎり</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-3.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-3.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Spaghetti &amp; Lasagna</span>
                    <span>スパゲッティとラザニア</span>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-1.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-1.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Fried Rice</span>
                    <span>チャーハン</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-2.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-2.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Onigiri</span>
                    <span>おにぎり</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-3.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-3.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Spaghetti &amp; Lasagna</span>
                    <span>スパゲッティとラザニア</span>
                </p>
            </div>
        </div>
    </div>
    
    <p>&nbsp;</p>
    
    <div class="container mc-body-container">
        <div class="bx-news-page">
            <a href="retail_product.php?page=1" <?php if( !isset($_GET["page"]) || (isset($_GET["page"]) && $_GET["page"]==1 ) ) { ?>class="active"<?php } ?>>
                1
            </a>
            <a href="retail_product.php?page=2" <?php if( isset($_GET["page"]) && $_GET["page"]==2 ) { ?>class="active"<?php } ?>>
                2
            </a>
            <a href="retail_product.php?page=3" <?php if( isset($_GET["page"]) && $_GET["page"]==3 ) { ?>class="active"<?php } ?>>
                3
            </a>
            <a href="retail_product.php?page=4" <?php if( isset($_GET["page"]) && $_GET["page"]==4 ) { ?>class="active"<?php } ?>>
                4
            </a>
            <a href="retail_product.php?page=5" <?php if( isset($_GET["page"]) && $_GET["page"]==5 ) { ?>class="active"<?php } ?>>
                5
            </a>
            <a href="retail_product.php?page=6" <?php if( isset($_GET["page"]) && $_GET["page"]==6 ) { ?>class="active"<?php } ?>>
                6
            </a>
        </div>
    </div>
</section>

<section class="mobile-wb bx-body-mobile">
    <div class="container mc-body-container">
        <div class="row">
            <div class="col-md-7 bx-about-us-history">
                <h2 class="header-t2">Our Retail Products</h2>
                <h4 class="sub-header-t2">私たちの小売製品</h4>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-1.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-1.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Fried Rice</span>
                    <span>チャーハン</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-2.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-2.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Onigiri</span>
                    <span>おにぎり</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-3.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-3.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Spaghetti &amp; Lasagna</span>
                    <span>スパゲッティとラザニア</span>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-1.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-1.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Fried Rice</span>
                    <span>チャーハン</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-2.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-2.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Onigiri</span>
                    <span>おにぎり</span>
                </p>
            </div>
            
            <div class="col-md-4 col-retail-product">
                <div class="product-img" style="background-image: url('assets/img/img-retail-product-3.jpg');">
                    <div>
                        <img src="assets/img/img-retail-product-hover-3.png" class="mw-100" />
                    </div>
                    <a href="retail_product_detail.php" class="btn-retprod">
                        <span>View Details</span>
                        <hr />
                    </a>
                </div>
                <p class="product-nm">
                    <span>Spaghetti &amp; Lasagna</span>
                    <span>スパゲッティとラザニア</span>
                </p>
            </div>
        </div>
    </div>
    
    <p>&nbsp;</p>
    
    <div class="container mc-body-container">
        <div class="bx-news-page">
            <a href="retail_product.php?page=1" <?php if( !isset($_GET["page"]) || (isset($_GET["page"]) && $_GET["page"]==1 ) ) { ?>class="active"<?php } ?>>
                1
            </a>
            <a href="retail_product.php?page=2" <?php if( isset($_GET["page"]) && $_GET["page"]==2 ) { ?>class="active"<?php } ?>>
                2
            </a>
            <a href="retail_product.php?page=3" <?php if( isset($_GET["page"]) && $_GET["page"]==3 ) { ?>class="active"<?php } ?>>
                3
            </a>
            <a href="retail_product.php?page=4" <?php if( isset($_GET["page"]) && $_GET["page"]==4 ) { ?>class="active"<?php } ?>>
                4
            </a>
            <a href="retail_product.php?page=5" <?php if( isset($_GET["page"]) && $_GET["page"]==5 ) { ?>class="active"<?php } ?>>
                5
            </a>
            <a href="retail_product.php?page=6" <?php if( isset($_GET["page"]) && $_GET["page"]==6 ) { ?>class="active"<?php } ?>>
                6
            </a>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>