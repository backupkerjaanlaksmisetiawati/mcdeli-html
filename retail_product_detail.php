<?php include 'templates/header.php'; ?>
    
<?php include 'templates/navigation.php'; ?>

<section class="desktop-wb">
    <div class="navigable rp-list" style="background-color: #FFBA76;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Salmon Mayo Spicy
                    </h2>
                    <img src="assets/img/rp-onigiri-salmon-mayo-spicy.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable rp-list" style="background-color: #FFA15F;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Tuna Mayo Spicy
                    </h2>
                    <img src="assets/img/rp-onigiri-tuna-mayo-spicy.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable rp-list" style="background-color: #fbbd80;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Salmon Mayo
                    </h2>
                    <img src="assets/img/rp-onigiri-salmon-mayo.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable rp-list" style="background-color: #FFA15F;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Tuna Mayo
                    </h2>
                    <img src="assets/img/rp-onigiri-tuna-mayo.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable rp-list" style="background-color: #FFBA76;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Onigiri Nasi Goreng
                    </h2>
                    <img src="assets/img/rp-onigiri-nasgor.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb">
    <div class="navigable-mobile rp-list" style="background-color: #FFBA76;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Salmon Mayo Spicy
                    </h2>
                    <img src="assets/img/rp-onigiri-salmon-mayo-spicy.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable-mobile rp-list" style="background-color: #FFA15F;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Tuna Mayo Spicy
                    </h2>
                    <img src="assets/img/rp-onigiri-tuna-mayo-spicy.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable-mobile rp-list" style="background-color: #fbbd80;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Salmon Mayo
                    </h2>
                    <img src="assets/img/rp-onigiri-salmon-mayo.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable-mobile rp-list" style="background-color: #FFA15F;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Tuna Mayo
                    </h2>
                    <img src="assets/img/rp-onigiri-tuna-mayo.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="navigable-mobile rp-list" style="background-color: #FFBA76;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="breadcrump">
                        Retail Product > <strong>Onigiri</strong>
                    </div>
                    <h1 class="title">
                        What is Onigiri?
                    </h1>
                    <div class="desc">
                        <p>
                            Onigiri is a Japanese dish consisting of 
                            small balls or triangles of rice stuffed 
                            with a pickled or salted filling, and 
                            typically wrapped in dried seaweed.
                        </p>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7 text-center">
                    <h2 class="name">
                        Onigiri Nasi Goreng
                    </h2>
                    <img src="assets/img/rp-onigiri-nasgor.jpg" class="rp-img" />
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'templates/footer.php'; ?>