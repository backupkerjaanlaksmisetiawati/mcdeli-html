    <section class="desktop-wb">
      <footer id="desktop-footer">
        <div>
          <div class="container">
            <div class="row">
              <div class="col-md-1">
                <img src="assets/img/logo-footer.jpg" />
              </div>
              
              <div class="col-md-9">
                <div class="d-flex justify-content-center bx-footer-menu">
                  <ul class="footer-menu-2">
                    <li>
                      <a href="about_us.php">
                        About Us
                      </a>
                    </li>
                    <li>
                      <a href="rd_production.php">
                        R&amp;D and Production
                      </a>
                    </li>
                    <li>
                      <a href="qa_qc.php">
                        QA &amp; QC
                      </a>
                    </li>
                    <li>
                      <a href="retail_product.php">
                        Retail Product
                      </a>
                    </li>
                    <li>
                      <a href="b2b_product.php">
                        B2B Product
                      </a>
                    </li>
                    <li>
                      <a href="news_event.php">
                        News &amp; Event
                      </a>
                    </li>
                    <li>
                      <a href="contact_us.php">
                        Contact Us
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="col-md-1">
                <img src="assets/img/logo-halal.jpg" />
              </div>
              
              <div class="col-md-1">
                <img src="assets/img/logo-certified.jpg" />
              </div>
            </div>
          </div>
        </div>
        <div class="copy-desktop">
          &copy; 2020. MCDelica. All rights reserved
        </div>
      </footer>
    </section>
    
    <section class="mobile-wb">
      <footer id="desktop-footer">
        <div>
          <div class="container">
            <div class="row justify-content-between">
              <div class="col-md-2" style="width: 29%;">
                <img src="assets/img/logo-footer.jpg" />
              </div>
              
              <div class="col-md-10" style="width: 35%;">
                <div class="d-flex justify-content-end bx-footer-menu">
                  <div>
                    <img src="assets/img/logo-halal.jpg" />
                  </div>
                  
                  <div>
                    <img src="assets/img/logo-certified.jpg" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="copy-desktop">
          &copy; 2020. MCDelica. All rights reserved
        </div>
      </footer>
    </section>
    
    <!-- used by all pages -->
    <script src="assets/js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
		<script src="assets/js/bootstrap4.3.1.bundle.min.js" crossorigin="anonymous"></script>
    <!-- used by all pages -->
    
    <!-- used by homepage, about us & news detail pages -->
    <script src="assets/js/owl.carousel.min.js"></script>
    <!-- used by homepage, about us & news detail pages -->

    <!-- used by contact us pages -->
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
    <!-- used by contact us pages -->

    <!-- used by detail retail product pages -->
    <script src="assets/js/page-content-navigation.js"></script>
    <!-- used by detail retail product pages -->

    <!-- used by all pages -->
    <script src="assets/js/scripts.js"></script>
    <!-- used by all pages -->
	</body>
</html>