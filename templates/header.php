<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<title>Mc Deli</title>

  <meta name="description" content="" />
  <meta name="author" content="Ryu Amy" />
  <meta http-equiv="cache-control" content="no-cache" />
 <meta name="apple-mobile-web-app-capable" content="yes" /> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1.0, user-scalable=0">
	<link rel="shortcut icon" type="image/png" href="assets/img/logo-mini.png" />

  <!-- used by all pages -->
  <link href="assets/css/styles_1.3.css" rel="stylesheet" />
  <script src="assets/js/font-awesome-5.11.2.all.min.js" crossorigin="anonymous"></script>
  <!-- used by all pages -->
  

  <!-- used by home, b2b product, about us, news & event detail page only -->
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
  <!-- used by home, b2b product, about us, news & event detail page only -->
  
  <!-- used by contact us page only -->
  <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
  <!-- used by contact us page only -->

  <!-- used by retail product detail page only -->
  <link rel="stylesheet" type="text/css" href="assets/css/page-content-navigation.css" /> 
  <!-- used by retail product detail page only -->

  </head>

  <body style="overflow-x:hidden !important;">
  <!-- close body on pages file -->