<?php $curr_page = basename($_SERVER["SCRIPT_FILENAME"], '.php'); ?>

<section class="desktop-wb bx-menu-2">
    <div class="container">
        <div class="row">
            <a href="index.php" class="header-logo col-md-2">
                <img src="assets/img/logo-mini.png" />
            </a>
            
            <div class="col-md-10">
                <nav class="menu-2">
                    <a href="about_us.php" class="<?php if($curr_page==="about_us") { ?>active<?php } ?>">
                        About Us
                        <span></span>
                    </a>
                    <a href="rd_production.php" class="<?php if($curr_page==="rd_production") { ?>active<?php } ?>">
                        R&amp;D and Production
                        <span></span>
                    </a>
                    <a href="qa_qc.php" class="<?php if($curr_page==="qa_qc") { ?>active<?php } ?>">
                        QA &amp; QC
                        <span></span>
                    </a>
                    <a href="retail_product.php" class="<?php if($curr_page==="retail_product" || $curr_page==="retail_product_detail") { ?>active<?php } ?>">
                        Retail Product
                        <span></span>
                    </a>
                    <a href="b2b_product.php" class="<?php if($curr_page==="b2b_product") { ?>active<?php } ?>">
                        B2B Product
                        <span></span>
                    </a>
                    <a href="news_event.php" class="<?php if($curr_page==="news_event" || $curr_page==="news_event_detail") { ?>active<?php } ?>">
                        News &amp; Event
                        <span></span>
                    </a>
                    <a href="contact_us.php" class="<?php if($curr_page==="contact_us") { ?>active<?php } ?>">
                        Contact Us
                        <span></span>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="mobile-wb position-relative">
    <div class="container">
        <div class="row">
            <div class="col-md-12 bx-menu-1">
                <a href="index.php">
                    <img src="assets/img/logo-mini.png" />
                </a>

                <div class="hamburger">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <nav class="menu-1">
        <a href="about_us.php" class="<?php if($curr_page==="about_us") { ?>active<?php } ?>">
            About Us
            <span></span>
        </a>
        <a href="rd_production.php" class="<?php if($curr_page==="rd_production") { ?>active<?php } ?>">
            R&amp;D and Production
            <span></span>
        </a>
        <a href="qa_qc.php" class="<?php if($curr_page==="qa_qc") { ?>active<?php } ?>">
            QA &amp; QC
            <span></span>
        </a>
        <a href="retail_product.php" class="<?php if($curr_page==="retail_product" || $curr_page==="retail_product_detail") { ?>active<?php } ?>">
            Retail Product
            <span></span>
        </a>
        <a href="b2b_product.php" class="<?php if($curr_page==="b2b_product") { ?>active<?php } ?>">
            B2B Product
            <span></span>
        </a>
        <a href="news_event.php" class="<?php if($curr_page==="news_event" || $curr_page==="news_event_detail") { ?>active<?php } ?>">
            News &amp; Event
            <span></span>
        </a>
        <a href="contact_us.php" class="<?php if($curr_page==="contact_us") { ?>active<?php } ?>">
            Contact Us
            <span></span>
        </a>
    </nav>
</section>